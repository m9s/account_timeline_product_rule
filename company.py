#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.backend import TableHandler
from trytond.transaction import Transaction
from trytond.pool import Pool


class Company(ModelSQL, ModelView):
    _name = 'company.company'

    def init(self, module_name):
        super(Company, self).init(module_name)
        cursor = Transaction().cursor
        table = TableHandler(cursor, self, module_name)

        # Migration from 2.1.2 move default account rules
        # to account_product_rule module
        config_values = {}
        if table.column_exist('default_account_revenue_rule'):
            cursor.execute('SELECT default_account_revenue_rule FROM "'
                + self._table +'"')
            with Transaction().set_user(0):
                for account_revenue_rule_id in cursor.fetchall():
                    config_values['default_account_revenue_rule'] = (
                        account_revenue_rule_id[0])
        if table.column_exist('default_account_expense_rule'):
            cursor.execute('SELECT default_account_expense_rule FROM "'
                + self._table +'"')
            with Transaction().set_user(0):
                for account_expense_rule_id in cursor.fetchall():
                    config_values['default_account_expense_rule'] = (
                        account_expense_rule_id[0])
        if config_values and TableHandler.table_exist(cursor,
                'account_account_rule_configuration'):
            config_obj = Pool().get('account.account.rule.configuration')
            config_obj.create(config_values)
            table.drop_column('default_account_revenue_rule', exception=True)
            table.drop_column('default_account_expense_rule', exception=True)

Company()
