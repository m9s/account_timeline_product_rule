# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Product Rule',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Artikel Regel',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds field fiscalyear to account rule lines
''',
    'description_de_DE': '''
    - Fügt Feld Geschäftsjahr zu den Kontenregeln hinzu.
''',
    'depends': [
        'account_timeline',
        'account_product_rule',
    ],
    'xml': [
    ],
    'translation': [
    ],
}
