#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL
from trytond.pyson import Eval, PYSONEncoder
from trytond.pool import Pool


class AccountRule(ModelSQL, ModelView):
    _name = 'account.account.rule'

    def _lookup_account(self, account_id, date):
        account_obj = Pool().get('account.account')

        account = super(AccountRule, self)._lookup_account(account_id, date)
        new_account = False
        if account:
            new_account_id = account_obj.get_account_by_date(account, date)
            new_account = account_obj.browse(new_account_id)
        return new_account

AccountRule()


class AccountRuleLine(ModelSQL, ModelView):
    _name = 'account.account.rule.line'

    def __init__(self):
        super(AccountRuleLine, self).__init__()

        # Field account
        self.account = copy.copy(self.account)
        if not self.account.domain is None:
            self.account.domain = []

        fiscalyear_domain = [('fiscalyear', '=', Eval('fiscalyear'))]
        pyson_fiscalyear_domain = PYSONEncoder().encode(fiscalyear_domain)
        pyson_account_domain = PYSONEncoder().encode(self.account.domain)
        if pyson_fiscalyear_domain not in pyson_account_domain:
            self.account.domain += [fiscalyear_domain]

        if 'fiscalyear' not in self.account.depends:
            self.account.depends += ['fiscalyear']

        # Field fiscalyear
        self.fiscalyear = copy.copy(self.fiscalyear)
        self.fiscalyear.required = True

        self._reset_columns()

AccountRuleLine()
